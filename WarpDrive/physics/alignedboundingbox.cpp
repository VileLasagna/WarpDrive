#include "WarpDrive/physics/alignedboundingbox.hpp"

#ifdef WIN32
    #include <Windows.h>
#endif //WIN32
#include <GL/gl.h>

#include "WarpDrive/basemaths/vec3.hpp"
#include "WarpDrive/basemaths/cliplinesegbox.hpp"
#include "WarpDrive/basemaths/matrix44.hpp"
#include "WarpDrive/basesystem/displaymanager.hpp"
#include "WarpDrive/basesystem/gltask.hpp"


AABBox::AABBox( float x_min, float x_max,
                float y_min, float y_max,
                float z_min, float z_max )
   :xmin{x_min}, xmax{x_max},
    ymin{y_min}, ymax{y_max},
    zmin{z_min}, zmax{z_max},
    colour{Colour(1,1,1,1)},
    wireframe{true}
{
    initVAO();
}

void AABBox::ensureContains(float x, float y, float z) noexcept
{
    xmin = xmin <= x ? xmin : x;
    xmax = xmax >= x ? xmax : x;
    ymin = ymin <= y ? ymin : y;
    ymax = ymax >= y ? ymax : y;
    zmin = zmin <= z ? zmin : z;
    zmax = zmax >= z ? zmax : z;
}

void AABBox::draw() const
{
    Matrix44 model;
    model.setTranslation(xmin,ymin,zmin);
    Matrix44 transform;
    transform.setScaling(std::abs(xmax - xmin), std::abs(ymax - ymin), std::abs(zmax - zmin));

    GLTaskQueue q;
    std::vector< GLPackage<void> > v;

    v.push_back(WDGL(UniformMatrix4fv, transformUniform, 1, GL_FALSE, transform.Elements().data() ));
    v.push_back(WDGL(UniformMatrix4fv, modelUniform, 1, GL_FALSE, model.Elements().data() ));
    v.push_back(WDGL(Uniform4f, ambientUniform, colour.R(), colour.G(), colour.B(), colour.A() ));

    for(auto& task: v)
    {
        pushGLTask(q, task);
    }
    Game::instance()->queueGLTasks(std::move(q));
    for(auto& task: v)
    {
        task.get();
    }

    if(wireframe)
    {
        VAO.draw("wire",true);
    }
    else
    {
        VAO.draw("tris",true);
    }

}

AABBox AABBox::makeOctChild(int octant) const
{
    // Centre of box
    float cx = (xmin + xmax) / 2.0f;
    float cy = (ymin + ymax) / 2.0f;
    float cz = (zmin + zmax) / 2.0f;

    float x_min = xmin, x_max = xmax, y_min = ymin, y_max = ymax, z_min = zmin, z_max = zmax;
    if (octant & 1)
    {
        x_min = cx;
    }
    else
    {
        x_max = cx;
    }
    if (octant & 2)
    {
        y_min = cy;
    }
    else
    {
        y_max = cy;
    }
    if (octant & 4)
    {
        z_min = cz;
    }
    else
    {
        z_max = cz;
    }

    return AABBox(x_min, x_max, y_min, y_max, z_min, z_max);
}

bool AABBox::contains(const Vec3f& v) const noexcept
{
    return (    v.X() >= xmin && v.X() < xmax &&
                v.Y() >= ymin && v.Y() < ymax &&
                v.Z() >= zmin && v.Z() < zmax );
}

bool intersects(const Capsule& c, const AABBox& box)
{
    AABBox B(box.MinX()-c.Radius(), box.MaxX() + c.Radius(), box.MinY()-c.Radius(), box.MaxY()+c.Radius(), box.MinZ() - c.Radius(), box.MaxZ() + c.Radius());
    LineSeg clipped = c.Path();
    return Clip(clipped,B);
}

void AABBox::initVAO()
{

    /*
     *
     *  VERTS
     *
     *         7--------6
     *        /|       /|
     *       / |      / |
     *      3--|-----2  |
     *      |  |     |  |
     *      |  4-----|--5
     *      | /      | /
     *      |/       |/
     *      0--------1
     *
     *
     *  FACES
     *
     *  A - front
     *  B - right
     *  C - top
     *  D - left
     *  E - bottom
     *  F - back
     *
     */

    VertexBuffer verts;
    ElementBuffer tris;

    tris.setPrimitive(ElementBuffer::DrawType::TRIS);
    tris.Info().setName("tris");


    //  0 - BOTTOM LEFT FRONT
    verts.push_back(Vertex(Vec3f(0,0,0),Vec3f(-1,-1,-1).normalise(), Vec2f( 0.25f, 0.332f),colour));
    //  1 - BOTTOM RIGHT FRONT
    verts.push_back(Vertex(Vec3f(1,0,0),Vec3f( 1,-1,-1).normalise(), Vec2f( 0.5f,  0.332f) ));
    //  2 - TOP RIGHT FRONT
    verts.push_back(Vertex(Vec3f(1,1,0),Vec3f( 1, 1,-1).normalise(), Vec2f( 0.5f,  0.665f) ));
    //  3 - TOP LEFT FRONT
    verts.push_back(Vertex(Vec3f(0,1,0),Vec3f(-1, 1,-1).normalise(), Vec2f( 0.25f, 0.665f) ));
    //  4 - BOTTOM LEFT BACK
    verts.push_back(Vertex(Vec3f(0,0,1),Vec3f(-1,-1, 1).normalise(), Vec2f( 0.75f,  0.332f) ));
    //  5 - BOTTOM RIGHT BACK
    verts.push_back(Vertex(Vec3f(1,0,1),Vec3f( 1,-1, 1).normalise(), Vec2f( 1.0f,  0.332f) ));
    //  6 - TOP RIGHT BACK
    verts.push_back(Vertex(Vec3f(1,1,1),Vec3f( 1, 1, 1).normalise(), Vec2f( 1.0f,  0.665f) ));
    //  7 - TOP LEFT BACK
    verts.push_back(Vertex(Vec3f(0,1,1),Vec3f(-1, 1, 1).normalise(), Vec2f( 0.75f,  0.665f)));

    VAO.Verts(std::move(verts));
    VAO.pushElementBuffer({ElementBuffer::DrawType::TRIS,
                          { 0,1,2,  2,3,0, //A
                            2,1,5,  5,6,2, //B
                            2,6,7,  7,3,2, //C
                            7,3,4,  3,0,4, //D
                            4,0,1,  1,5,4, //E
                            7,4,5,  5,6,4, //F
                          },
                          "tris"});


    ElementBuffer wire(ElementBuffer::DrawType::LINES,
                       {  0,1,   1,2,   2,3,   3,0, //A
                          1,5,   5,6,   6,2,        //B
                          7,3,   6,7,   7,3,        //C
                          0,4,   4,7,               //D
                          4,5                       //E
                                                    //F
                       },
                       "wire");


    VAO.pushElementBuffer(std::move(wire));
    VAO.Load();

}
