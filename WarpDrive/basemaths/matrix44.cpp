
#include "WarpDrive/basemaths/matrix44.hpp"
#include "WarpDrive/basesystem/err.hpp"

#ifdef WIN32
    #include <Windows.h>
#endif //WIN32

#include <GL/gl.h>
#include <algorithm>

#include <iostream>

//   PI/180

constexpr float DEGTORAD = 0.017453292519943295769f;

void Matrix44::setIdentity()
{
    Identity(Front());
}

Matrix44::Matrix44()
{
    Identity(Front());
    Identity(Back());
}

Matrix44::Matrix44(const Matrix44& other)
{
    std::copy(other.cFront().begin(), other.cFront().end(), Front().begin());
    Identity(Back());
}

Matrix44::Matrix44(const Matrix44&& other)
{
    std::copy(other.cFront().begin(), other.cFront().end(), Front().begin());
    Identity(Back());
}

Matrix44::Matrix44(float16& mat)
{
    Front() = mat;
    Identity(Back());
}

Matrix44::Matrix44(GLdouble* mat)
{
    for(size_t i = 0; i < 16; i++)
    {
        front[i] = static_cast<float>(mat[i]);
    }
    back.fill(0);
}

Matrix44::Matrix44(GLfloat *mat)
{
    for(size_t i = 0; i < 16; i++)
    {
        front[i] = static_cast<float>(mat[i]);
    }
    back.fill(0);
}

GLfloat16 Matrix44::toGLfloat16() const noexcept
{
    GLfloat16 ret;
    for(ushort i = 0; i < 16; i++)
    {
        ret[i] = front[i];
    }
    return ret;
}

GLdouble16 Matrix44::toGLdouble16() const noexcept
{
    GLdouble16 ret;
    for(ushort i = 0; i < 16; i++)
    {
        ret[i] = front[i];
    }
    return ret;
}

void Matrix44::multiply(const Matrix44 &rhs)
{



    Back()[0]  = Front()[0]*rhs.cFront()[0]  + Front()[4]*rhs.cFront()[1]  + Front()[8] *rhs.cFront()[2]  + Front()[12]*rhs.cFront()[3];
    Back()[1]  = Front()[1]*rhs.cFront()[0]  + Front()[5]*rhs.cFront()[1]  + Front()[9] *rhs.cFront()[2]  + Front()[13]*rhs.cFront()[3];
    Back()[2]  = Front()[2]*rhs.cFront()[0]  + Front()[6]*rhs.cFront()[1]  + Front()[10]*rhs.cFront()[2]  + Front()[14]*rhs.cFront()[3];
    Back()[3]  = Front()[3]*rhs.cFront()[0]  + Front()[7]*rhs.cFront()[1]  + Front()[11]*rhs.cFront()[2]  + Front()[15]*rhs.cFront()[3];
    Back()[4]  = Front()[0]*rhs.cFront()[4]  + Front()[4]*rhs.cFront()[5]  + Front()[8] *rhs.cFront()[6]  + Front()[12]*rhs.cFront()[7];
    Back()[5]  = Front()[1]*rhs.cFront()[4]  + Front()[5]*rhs.cFront()[5]  + Front()[9] *rhs.cFront()[6]  + Front()[13]*rhs.cFront()[7];
    Back()[6]  = Front()[2]*rhs.cFront()[4]  + Front()[6]*rhs.cFront()[5]  + Front()[10]*rhs.cFront()[6]  + Front()[14]*rhs.cFront()[7];
    Back()[7]  = Front()[3]*rhs.cFront()[4]  + Front()[7]*rhs.cFront()[5]  + Front()[11]*rhs.cFront()[6]  + Front()[15]*rhs.cFront()[7];
    Back()[8]  = Front()[0]*rhs.cFront()[8]  + Front()[4]*rhs.cFront()[9]  + Front()[8] *rhs.cFront()[10] + Front()[12]*rhs.cFront()[11];
    Back()[9]  = Front()[1]*rhs.cFront()[8]  + Front()[5]*rhs.cFront()[9]  + Front()[9] *rhs.cFront()[10] + Front()[13]*rhs.cFront()[11];
    Back()[10] = Front()[2]*rhs.cFront()[8]  + Front()[6]*rhs.cFront()[9]  + Front()[10]*rhs.cFront()[10] + Front()[14]*rhs.cFront()[11];
    Back()[11] = Front()[3]*rhs.cFront()[8]  + Front()[7]*rhs.cFront()[9]  + Front()[11]*rhs.cFront()[10] + Front()[15]*rhs.cFront()[11];
    Back()[12] = Front()[0]*rhs.cFront()[12] + Front()[4]*rhs.cFront()[13] + Front()[8] *rhs.cFront()[14] + Front()[12]*rhs.cFront()[15];
    Back()[13] = Front()[1]*rhs.cFront()[12] + Front()[5]*rhs.cFront()[13] + Front()[9] *rhs.cFront()[14] + Front()[13]*rhs.cFront()[15];
    Back()[14] = Front()[2]*rhs.cFront()[12] + Front()[6]*rhs.cFront()[13] + Front()[10]*rhs.cFront()[14] + Front()[14]*rhs.cFront()[15];
    Back()[15] = Front()[3]*rhs.cFront()[12] + Front()[7]*rhs.cFront()[13] + Front()[11]*rhs.cFront()[14] + Front()[15]*rhs.cFront()[15];

    Flip();

}

void Matrix44::print() const
{
    std::array<std::string,16> strings;
    for(size_t i = 0; i <16; i++)
    {
        float val = (*this)[i];
        std::string& current = strings[i];
        current.append(std::to_string(val));
        if (current.at(0)  != '-')
        {
            current = " ";
            current.append(std::to_string(val));
        }

        while(current.length() < 9)
        {
        current.append(" ");
        }
    }
    std::cout << "⎡ " << strings[0] << " , " << strings[4] <<" , " << strings[8] <<" , " << strings[12] <<"  ⎤" << std::endl;
    std::cout << "⎢ " << strings[1] << " , " << strings[5] <<" , " << strings[9] <<" , " << strings[13] <<"  ⎢" << std::endl;
    std::cout << "⎢ " << strings[2] << " , " << strings[6] <<" , " << strings[10]<<" , " << strings[14] <<"  ⎢" << std::endl;
    std::cout << "⎣ " << strings[3] << " , " << strings[7] <<" , " << strings[11]<<" , " << strings[15] <<"  ⎦" << std::endl;
}

void Matrix44::setRotation(float degx, float degy, float degz, bool clear)
{


    float radx = degx* DEGTORAD;
    float rady = degy* DEGTORAD;
    float radz = degz* DEGTORAD;

    setRotationRad(radx,rady,radz, clear);
}

void Matrix44::applyMatrix() const
{
    //glMultMatrixf(cFront().data());
}

void Matrix44::setMatrix() const
{
//    glLoadIdentity();
//    applyMatrix();
}


void Matrix44::setRotationRad(float radx, float rady, float radz, bool clear)
{
    if(clear)
    {
        Identity(Back());
    }
    else
    {
        Shadow();
    }

    float cy = static_cast<float>( cos(static_cast<double>(rady)) );
    float cx = static_cast<float>( cos(static_cast<double>(radx)) );
    float cz = static_cast<float>( cos(static_cast<double>(radz)) );
    float sy = static_cast<float>( sin(static_cast<double>(rady)) );
    float sx = static_cast<float>( sin(static_cast<double>(radx)) );
    float sz = static_cast<float>( sin(static_cast<double>(radz)) );

    float cxsy = cx*sy;
    float sxsy = sx*sy;

//    Back()[0] = (cy*cz);
//    Back()[1] = (cz*sx*sy) - (cx*sz);
//    Back()[2] = (cx*cz*sy) + (sx*sz);

//    Back()[4] = (cy*sz);
//    Back()[5] = (cx*cz) + (sx*sy*sz);
//    Back()[6] = (cx*sy*sz) - (cz*sx);

//    Back()[8] = -sy;
//    Back()[9] = (cy*sx);
//    Back()[10] = (cx*cy);


    // http://www.opengl-tutorial.org/assets/faq_quaternions/index.html#Q26
    // Warning: This tutorial assumes row-major
    Back()[0] = cy * cz;
    Back()[1] = ( sxsy * cz) + (cx * sz);
    Back()[2] = (-cxsy * cz) + (sx * sz);

    Back()[4] = -cy * sz;
    Back()[5] = (-sxsy * sz) + (cx * cz);
    Back()[6] = ( cxsy * sz) + (sx * cz);

    Back()[8]  =  sy;
    Back()[9]  = -sx * cy;
    Back()[10] =  cx * cy;

    Flip();

}

void Matrix44::setRotation(float deg, Vec3f vector, bool clear)
{
    if(WrpDrv::flZero(deg))
    {
        if(clear)
        {
            setIdentity();
        }
        return;
    }
    setRotationRad(deg* DEGTORAD, vector, clear);
}

void Matrix44::setRotationRad(float rads, Vec3f vector, bool clear)
{
    if(clear)
    {
        Identity(Back());
    }
    else
    {
        Shadow();
    }

    vector.normalise();
    float16 el =
    {
     { 0,
      -vector.Z() * static_cast<float>( sin(static_cast<double>(rads))),
       vector.Y() * static_cast<float>( sin(static_cast<double>(rads))),
       0,

       vector.Z() * static_cast<float>( sin(static_cast<double>(rads))),
       0,
      -vector.X() * static_cast<float>( sin(static_cast<double>(rads))),
       0,

      -vector.Y() * static_cast<float>( sin(static_cast<double>(rads))),
       vector.X() * static_cast<float>( sin(static_cast<double>(rads))),
       0,
       0,

       0,
       0,
       0,
       1 * static_cast<float>( sin(static_cast<double>(rads)) )
     }
    };
    //Add the values to Back()
    std::transform( Back().begin(), Back().end(),
                    el.begin(), Back().begin(),
                    std::plus<float>());
    Flip();
}

void Matrix44::setPerspective(float fovy, float aspectratio, float znear, float zfar)
{

    /*
     * I just reimplemented the internal calculations of gluPerspective
     *
     * From https://www.khronos.org/registry/OpenGL-Refpages/gl2.1/xhtml/gluPerspective.xml
     *
     * gluPerspective specifies a viewing frustum into the world coordinate system. In general,
     * the aspect ratio in gluPerspective should match the aspect ratio of the associated viewport.
     * For example, aspect = 2.0 means the viewer's angle of view is twice as wide in x as it is in y.
     * If the viewport is twice as wide as it is tall, it displays the image without distortion.
     *
     * Given f defined as follows:
     *
     * f = cotangent(fovy/2)
     *
     * The generated matrix is
     *
     *      ⎡   f/aspect    0               0                           0               ⎤
     *      ⎢       0       f               0                           0               ⎢
     *      ⎢       0       0   (zFar+zNear)/(zNear-zFar)   (2*zFar*zNear)/(zNear-zFar) ⎢
     *      ⎣       0       0              -1                           0               ⎦
     *
     */

    Identity(Back());
    auto angle = fovy * DEGTORAD * 0.5f;
    float f = static_cast<float>( cos(angle)/sin(angle));

    Back()[0]  =  f / aspectratio;
    Back()[5]  = f;
    Back()[10] = (znear + zfar) / (znear - zfar);
    Back()[11] = -1.f;
    Back()[14] = (2.f * zfar * znear) / (znear - zfar);
    Back()[15] = 0.f;

    Flip();
}

void Matrix44::lookAt(const Vec3f eye, const Vec3f target, const Vec3f up)
{

    /* This is a reimplementation of gluLookAt as explained in:
     *
     * https://www.khronos.org/registry/OpenGL-Refpages/gl2.1/xhtml/gluLookAt.xml
     *
     *
     * gluLookAt creates a viewing matrix derived from an eye point,
     * a reference point indicating the center of the scene, and an UP vector.
     *
     * The matrix maps the reference point to the negative z axis and the eye
     * point to the origin. When a typical projection matrix is used, the center
     * of the scene therefore maps to the center of the viewport. Similarly, the
     * direction described by the UP vector projected onto the viewing plane is
     * mapped to the positive y axis so that it points upward in the viewport.
     * The UP vector must not be parallel to the line of sight from the eye point
     * to the reference point.
     *
     * Let
     *
     *      ⎡ centerX - eyeX ⎤
     *  F = ⎢ centerY - eyeY ⎢
     *      ⎣ centerZ - eyeZ ⎦
     *
     *      ⎡ upX ⎤
     * UP = ⎢ upY ⎢
     *      ⎣ upZ ⎦
     *
     * Then normalize as follows:
     * f = F/F
     *
     * UP″ = UP/UP
     *
     * Finally, let s = f × UP ″ , and u = (s/s) × f
     *
     * M is then constructed as follows:
     *
     *      ⎡   s0   s1  s2 0 ⎤
     *  M = ⎢   u0   u1  u2 0 ⎢
     *      ⎢  -f0  -f1 -f2 0 ⎢
     *      ⎣   0    0   0  1 ⎦
     *
     * and gluLookAt is equivalent to
     * glMultMatrixf(M);
     * glTranslated(-eyex, -eyey, -eyez);
     *
     */

    Identity(Back());
    Vec3f fn = target - eye;
    fn.normalise();
    Vec3f UP = up;
    UP.normalise();
    Vec3f s = crossProd(fn, UP);
    Vec3f sn = s;
    sn.normalise();
    Vec3f u = crossProd(sn, fn);
    fn *= -1;


    Back()[0]  = s.X();
    Back()[1]  = u.X();
    Back()[2]  = fn.X();
    //Back()[3]  = 0;

    Back()[4]  = s.Y();
    Back()[5]  = u.Y();
    Back()[6]  = fn.Y();
    //Back()[7]  = 0;

    Back()[8]  = s.Z();
    Back()[9]  = u.Z();
    Back()[10] = fn.Z();
    //Back()[11] = 0;

    Back()[12] = dotProd(s,-eye);;
    Back()[13] = dotProd(u,-eye);;
    Back()[14] = dotProd(fn,-eye);;
    //Back()[15] = 1.0f;


    Flip();
}

const float16& Matrix44::Elements() const
{
    return spin?back:front;
}

const float& Matrix44::operator[](size_t idx) const
{
    if(idx >= 16)
    {
        throw std::runtime_error("Accessing invlaid index in Matrix44");
    }
    return Elements()[idx];
}

void Matrix44::setTranslation(float x, float y, float z, bool clear)
{
    if(clear)
    {
        Identity(Back());
    }
    else
    {
        Shadow();
    }

    Back()[12] += x;
    Back()[13] += y;
    Back()[14] += z;

    Flip();
}

void Matrix44::setScaling(float x, float y, float z)
{
#ifndef NDEBUG
    //this is a silly check no one wants to make on a release build
    if(x < 0)
    {
        Err::report("WARNING::MATRIX:: Scaling by a negative value. Probably an error");
        return;
    }
#endif

    Identity(Back());
    Back()[0]  = x;
    Back()[5]  = y < 0 ? x:y;
    Back()[10] = z < 0 ? x:z;

    Flip();
}

void Matrix44::getModelview()
{
//    glGetFloatv(GL_MODELVIEW_MATRIX, Back().data());
//    Flip();
}


void Matrix44::getProjection()
{
//    glGetFloatv(GL_MODELVIEW_MATRIX, Back().data());
//    Flip();
}

Matrix44 Matrix44::operator *(const Matrix44& rhs)
{
    Matrix44 result(*this);
    result.multiply(rhs);
    return result;
}

Matrix44& Matrix44::operator *=(const Matrix44 &rhs)
{
    multiply(rhs);
    return *this;
}

Matrix44 Matrix44::operator +(const Matrix44& rhs)
{
    float16 res;
    for(size_t i = 0; i < 16; i++)
    {
        res[i] = Front()[i] + rhs.cFront()[i];
    }
    return Matrix44(res);
}

Matrix44& Matrix44::operator +=(const Matrix44& rhs)
{
    Shadow();
    for(size_t i = 0; i < 16; i++)
    {
        Back()[i] += rhs.cFront()[i];
    }
    Flip();
    return *this;
}

Matrix44 Matrix44::operator *(float scalar)
{
    float16 res;
    for(size_t i = 0; i < 16; i++)
    {
        res[i] = Front()[i] * scalar;
    }
    return Matrix44(res);
}


Matrix44& Matrix44::operator *=(float scalar)
{
    Shadow();
    for(size_t i = 0; i < 16; i++)
    {
        Back()[i] *= scalar;
    }
    Flip();
    return *this;
}

Matrix44 Matrix44::operator +(float scalar)
{
    float16 res;
    for(size_t i = 0; i < 16; i++)
    {
        res[i] = Front()[i] + scalar;
    }
    return Matrix44(res);
}

Matrix44& Matrix44::operator +=(float scalar)
{
    Shadow();
    for(size_t i = 0; i < 16; i++)
    {
        Back()[i] += scalar;
    }
    Flip();
    return *this;
}

Matrix44 Matrix44::operator /(float scalar)
{
    float16 res;
    for(size_t i = 0; i < 16; i++)
    {
        res[i] = Front()[i] / scalar;
    }
    return Matrix44(res);
}


Matrix44& Matrix44::operator /=(float scalar)
{
    Shadow();
    for(size_t i = 0; i < 16; i++)
    {
        Back()[i] /= scalar;
    }
    Flip();
    return *this;
}

Matrix44 Matrix44::operator -(float scalar)
{
    float16 res;
    for(size_t i = 0; i < 16; i++)
    {
        res[i] = Front()[i] - scalar;
    }
    return Matrix44(res);
}

Matrix44& Matrix44::operator -=(float scalar)
{
    Shadow();
    for(size_t i = 0; i < 16; i++)
    {
        Back()[i] -= scalar;
    }
    Flip();
    return *this;
}

Matrix44& Matrix44::operator =(const Matrix44& rhs)
{
    for(size_t i = 0; i < 16; i++)
    {
        Back()[i] = rhs.cFront()[i];
    }
    Flip();
    return *this;
}

Matrix44& Matrix44::operator =(const Matrix44&& rhs)
{
    for(size_t i = 0; i < 16; i++)
    {
        Back()[i] = rhs.cFront()[i];
    }
    Flip();
    return *this;
}

float16 &Matrix44::Front()
{
    return spin?back:front;
}

float16 &Matrix44::Back()
{
    return spin?front:back;
}

const float16 &Matrix44::cFront() const
{
    return spin?back:front;
}

const float16 &Matrix44::cBack() const
{
    return spin?front:back;
}

void Matrix44::Flip()
{
    spin.store(spin?false:true);
}

void Matrix44::Identity(float16 &f)
{
    f.fill(0);

    f[0] = 1;
    f[5] = 1;
    f[10] = 1;
    f[15] = 1;

}

void Matrix44::Shadow()
{
    std::copy(Front().begin(), Front().end(), Back().begin());
}
