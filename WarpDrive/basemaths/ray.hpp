#ifndef WD_RAY_HPP_DEFINED
#define WD_RAY_HPP_DEFINED

#include "WarpDrive/basemaths/vec3.hpp"
#include "WarpDrive/basemaths/lineseg.hpp"
#include "WarpDrive/display/vertexarray.hpp"


#include "WarpDrive/basemaths/sphere.hpp"
#include "WarpDrive/physics/alignedboundingbox.hpp"

class Ray
{

public:

    Ray(Vec3f o = Vec3f(0,0,0) , Vec3f d = Vec3f(0,0,1)) : origin{o}, direction{d.normalise()}, colour{1.0f,0.3f,0.8f,1.0f} {initVAO();}
    Ray(LineSeg segment) : origin{segment.Start()}, direction{(segment.End() - segment.Start()).normalise()}, colour{1.0f,0.3f,0.8f,1.0f} {initVAO();}

    Vec3f Origin() const noexcept {return origin;}
    Vec3f Direction() const noexcept {return direction;}

    void setOrigin(Vec3f& newOrigin) noexcept {origin = newOrigin;}
    void setOrigin(Vec3f&& newOrigin) noexcept {origin = newOrigin;}

    void setDirection(Vec3f& newDirection) noexcept {direction = newDirection;}
    void setDirection(Vec3f&& newDirection) noexcept {direction = newDirection;}

    void setModelUniform(GLint i) noexcept { modelUniform = i;}
    void setTransformUniform(GLint i) noexcept { transformUniform = i;}
    void setAmbientUniform(GLint i) noexcept { ambientUniform = i;}

    GLint ModelUniform() noexcept { return modelUniform;}
    GLint TransformUniform() noexcept { return transformUniform;}
    GLint AmbientUniform() noexcept { return ambientUniform;}

    bool collides(const Sphere &s) const;

    LineSeg getSegment(float start, float end) const noexcept;

    Vec3f pointAt(float distance) const noexcept;

    void draw() const;

private:

    void initVAO();

    Vec3f origin;
    Vec3f direction;
    VertexArray VAO;
    GLint modelUniform;
    GLint transformUniform;
    GLint ambientUniform;
    Colour colour;

};

#endif // WD_RAY_HPP_DEFINED
