#include "WarpDrive/basemaths/ray.hpp"

#include <iostream>

#include "WarpDrive/basemaths/matrix44.hpp"
#include "WarpDrive/basesystem/displaymanager.hpp"
#include "WarpDrive/basesystem/gltask.hpp"

bool Ray::collides(const Sphere &s) const
{
    Vec3f m = origin - s.Centre();
    float b = dotProd(m, direction);
    float c = dotProd(m, m) - (s.Radius() * s.Radius());
    float distAlongRay = 0;
    Vec3f collisionPoint;

    // Exit if r’s origin outside s (c > 0) and r pointing away from s (b > 0)
    if (c > 0.0f && b > 0.0f) return false;
    float discr = b*b - c;

    // A negative discriminant corresponds to ray missing sphere
    if (discr < 0.0f) return false;

    // Ray now found to intersect sphere, compute smallest t value of intersection
    distAlongRay = -b - std::sqrt(discr);

    // If t is negative, ray started inside sphere so clamp t to zero
    if (distAlongRay < 0.0f) distAlongRay = 0.0f;
    collisionPoint = origin + (direction * distAlongRay);

    std::cout << "Collision detected" << std::endl;
    return true;
}

LineSeg Ray::getSegment(float start, float end) const noexcept
{
    return LineSeg(pointAt(start), pointAt(end));
}

Vec3f Ray::pointAt(float distance) const noexcept
{
    return Vec3f{origin + (direction*distance)};
}

void Ray::draw() const
{
    Matrix44 model;
    model.setTranslation(origin.X(),origin.Y(),origin.Z());
    Matrix44 transform;
    transform.setScaling(1000.f);

    GLTaskQueue q;
    std::vector< GLPackage<void> > v;

    v.push_back(WDGL(UniformMatrix4fv, transformUniform, 1, GL_FALSE, transform.Elements().data() ));
    v.push_back(WDGL(UniformMatrix4fv, modelUniform, 1, GL_FALSE, model.Elements().data() ));
    v.push_back(WDGL(Uniform4f, ambientUniform, colour.R(), colour.G(), colour.B(), colour.A() ));

    for(auto& task: v)
    {
        pushGLTask(q, task);
    }
    Game::instance()->queueGLTasks(std::move(q));
    for(auto& task: v)
    {
        task.get();
    }

    VAO.draw("wire",true);
}

void Ray::initVAO()
{
    VertexBuffer verts;
    ElementBuffer wire{ElementBuffer::DrawType::LINES,{0,1},"wire"};
    verts.push_back(Vertex(Vec3f(0,0,0),Vec3f(0,0,1),Vec2f(0,0)));
    verts.push_back(Vertex(direction));
    VAO.pushElementBuffer(std::move(wire));
    VAO.Verts(std::move(verts));
    VAO.Load();
}
