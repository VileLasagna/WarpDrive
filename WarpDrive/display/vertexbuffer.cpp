#include "WarpDrive/display/vertexbuffer.hpp"

#include <assert.h>
#include "GL/glew.h"

#include <iostream>

#include "WarpDrive/basesystem/gltask.hpp"


void VertexBuffer::registerBuffers(std::vector<VertexBuffer> buffers)
{
    std::vector<unsigned int> indexes(buffers.size());
    auto pack = WDGL(GenBuffers, static_cast<int>(indexes.size()), indexes.data());
    Game::instance()->queueGLTask(pack.Task());
    pack.get();

    for(size_t i = 0; i < indexes.size(); i++)
    {
        assert((buffers[i].id == 0) && "VertexBuffer already registered!");
        buffers[i].id = indexes[i];
    }
}

VertexBuffer::VertexBuffer()
   :data(),
    id{0},
    type{DataType::STATIC}
{}

void VertexBuffer::push_back(Vertex& v)
{
   data.push_back(v);
}
void VertexBuffer::push_back(Vertex&& v)
{
    data.push_back(v);
}

void VertexBuffer::clear()
{
    data.clear();
}

void VertexBuffer::bindBuffer()
{
    GLTaskQueue q;
    std::vector< GLPackage<void> > v;
    if(!Registered())
    {
        auto task = WDGL(GenBuffers, 1, &id);
        Game::instance()->queueGLTask(task.Task());
        task.get();
    }

    v.push_back( WDGL(BindBuffer, GL_ARRAY_BUFFER, id));

    v.push_back( WDGL(BufferData,    GL_ARRAY_BUFFER,
                        static_cast<GLsizeiptr>(sizeof(Vertex)*data.size()),
                        data.data(),
                        static_cast<GLenum>(type)   ));
    for(auto& task: v)
    {
        pushGLTask(q, task);
    }
    Game::instance()->queueGLTasks(std::move(q));
    for(auto& task: v)
    {
        task.get();
    }

}

void VertexBuffer::bindBuffer(std::vector< GLPackage<void> >& v)
{

    if(!Registered())
    {
        auto task = WDGL(GenBuffers, 1, &id);
        Game::instance()->queueGLTask(task.Task());
        task.get();
    }

    v.push_back( WDGL(BindBuffer, GL_ARRAY_BUFFER, id));

    v.push_back( WDGL(BufferData,    GL_ARRAY_BUFFER,
                        static_cast<GLsizeiptr>(sizeof(Vertex)*data.size()),
                        data.data(),
                        static_cast<GLenum>(type)   ));

}
