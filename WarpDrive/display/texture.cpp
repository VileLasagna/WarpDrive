#include "WarpDrive/display/texture.hpp"
#include "WarpDrive/basesystem/err.hpp"
#ifdef WIN32
    #include <Windows.h>
#endif //WIN32
#include <GL/glu.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#include <assert.h>

#include "WarpDrive/basesystem/gltask.hpp"

WDTexture::WDTexture():
    textureId{0},
    initialized{false}
{}

bool WDTexture::loadTexture(const std::string& filename, bool unflipY)
{

    SDL_Surface* surf = IMG_Load(filename.c_str());
    if(surf == nullptr)
    {
        Err::notify("Failed to load texture -> " + filename + '\n' +"With error -> " + IMG_GetError());
        return false;
    }

    createFromSDLSurface(surf, unflipY);

    //SDL_FreeSurface(surf);

    return true;
}

bool WDTexture::createFromSDLSurface(SDL_Surface* surf, bool flipY)
{
    if (!surf)
    {
        Err::notify("Surface pointer invalid while creating Texture");
        return false;
    }

    GLTaskQueue q;
    std::vector< GLPackage<void> > v;

    auto pack = WDGL(GenTextures, 1, &textureId);
    Game::instance()->queueGLTask(pack.Task());
    pack.get();

    v.push_back(WDGL(BindTexture, GL_TEXTURE_2D, textureId));

    v.push_back(WDGL(TexParameterf, GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST));
    v.push_back(WDGL(TexParameterf, GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST));



//#pragma warning("TODO - URGENT")
#pragma message "TODO - URGENT"
    //SDL_SetAlpha(surf,SDL_SRCALPHA|SDL_RLEACCEL,128);
    SDL_SetSurfaceAlphaMod(surf, 128);
    unsigned char* flipped = nullptr;
    unsigned char* data = static_cast<unsigned char*>(surf->pixels);
    int width = surf->w;
    int height = surf->h;
    if(flipY)
    {
        flipped = new unsigned char[static_cast<size_t>(width * height * surf->format->BytesPerPixel)];
        for(int row = (height-1); row >= 0; row --)
        {
            for(int column = 0; column < surf->pitch ; column ++ )
            {
                flipped[(surf->pitch *row)+column] =
                        static_cast<unsigned char*>(surf->pixels)[(surf->pitch*((height-1) - row)) + column] ;
            }
        }
        data = flipped;
    }


    if (surf->format->BytesPerPixel == 3)
    {
        v.push_back(WDGL(TexImage2D, GL_TEXTURE_2D,
            0,
            GL_RGB,
            width,
            height,
            0,
            GL_RGB,
            GL_UNSIGNED_BYTE,
            data));

        v.push_back(WDGL(GenerateMipmap, GL_TEXTURE_2D));

//        gluBuild2DMipmaps(
//            GL_TEXTURE_2D,
//            GL_RGB,
//            width,
//            height,
//            GL_RGB,
//            GL_UNSIGNED_BYTE,
//            data);
    }
    else if (surf->format->BytesPerPixel == 4)
    {
        v.push_back(WDGL(TexImage2D, GL_TEXTURE_2D,
            0,
            GL_RGBA,
            width,
            height,
            0,
            GL_RGBA,
            GL_UNSIGNED_BYTE,
            data));

        v.push_back(WDGL(GenerateMipmap, GL_TEXTURE_2D));

//        gluBuild2DMipmaps(
//            GL_TEXTURE_2D,
//            GL_RGBA,
//            width,
//            height,
//            GL_RGBA,
//            GL_UNSIGNED_BYTE,
//            data);
    }
    else
    {
        // Unexpected format
        Err::notify("Surface depth invalid while creating Texture");
        assert(0);
    }
    if(flipped)
    {
        delete[] flipped;
        flipped = nullptr;
    }

    for(auto& task: v)
    {
        pushGLTask(q, task);
    }
    Game::instance()->queueGLTasks(std::move(q));
    for(auto& task: v)
    {
        task.get();
    }

    return true;
}

WDTexture::~WDTexture()
{
    if (initialized)
    {
        WDGL(DeleteTextures, 1, &textureId).get();
    }
}

void WDTexture::useThisTexture() const
{
    if(initialized)
    {
        #ifndef __EMSCRIPTEN__
            WDGL(Enable, GL_TEXTURE_2D).get();
        #endif
        WDGL(BindTexture, GL_TEXTURE_2D, textureId).get();
    }
}

void WDTexture::useNoTexture()
{
#ifndef __EMSCRIPTEN__
    WDGL(Disable,GL_TEXTURE_2D).get();
#endif
}
