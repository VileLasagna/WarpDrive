#include "WarpDrive/display/vertexarray.hpp"

#include "WarpDrive/basesystem/err.hpp"
#include "WarpDrive/basesystem/displaymanager.hpp"
#include "WarpDrive/basesystem/gltask.hpp"

#include <GL/glew.h>

VertexArray::VertexArray()
    :id{0},
     ebid{0},
     buffer{},
     indices{},
     type{VertexBuffer::DataType::STATIC}
{}

void VertexArray::pushElementBuffer(ElementBuffer::Data&& EBO)
{
    size_t offset = indices.size()*sizeof(unsigned int);
    EBOData.emplace_back(offset);
    EBOData.back().setLength(EBO.size());
    appendIndices(EBO);

}

void VertexArray::pushElementBuffer(ElementBuffer &&EBO)
{
    EBOData.push_back(EBO.Info());
    EBOData.back().setOffset(indices.size()*sizeof(unsigned int));
    EBOData.back().setLength(EBO.Indices().size());
    appendIndices(EBO.Indices());
}

void VertexArray::Verts(VertexBuffer &&VBO)
{
    buffer = VBO;
}

void VertexArray::draw(std::string buffer, bool autobind) const noexcept
{
    GLTaskQueue q;
    std::vector< GLPackage<void> > v;

    if(autobind)
    {
        v.push_back(WDGL(BindVertexArray,id));
       // DisplayManager::checkGLError("VertexArray::draw() -> glBindVertexArray(" + std::to_string(id)+")");
        for(ElementBuffer::Header h: EBOData)
        {
            if((buffer == "") || h.Name() == buffer )
            {
                v.push_back(WDGL(DrawElements,  static_cast<GLenum>(h.Primitive())      //mode
                                 , static_cast<GLsizei>(h.Length())        //count
                                 , GL_UNSIGNED_INT                         //type
                                 , reinterpret_cast<void*>(h.Offset())));   //indices
                //DisplayManager::checkGLError("VertexArray::draw() -> glDrawElements()");

            }
        }
        v.push_back(WDGL(BindVertexArray, 0));
        //DisplayManager::checkGLError("VertexArray::draw() -> glBindVertexArray(0)");

    }
    else
    {
        for(ElementBuffer::Header h: EBOData)
        {
            if((buffer == "") || h.Name() == buffer )
            {
                v.push_back(WDGL(DrawElements, static_cast<GLenum>(h.Primitive())
                                 , static_cast<GLsizei>(h.Length())
                                 , GL_UNSIGNED_INT
                                 , reinterpret_cast<void*>(h.Offset())));
                //DisplayManager::checkGLError("VertexArray::draw() -> glDrawElements()");
            }
        }
    }
    for(auto& task: v)
    {
        pushGLTask(q, task);
    }
    Game::instance()->queueGLTasks(std::move(q));
    for(auto& task: v)
    {
        task.get();
    }
}

void VertexArray::Load()
{
    if(id != 0)
    {
        #ifndef NDEBUG
        Err::log("WARNING::VERTEXARRAY:: Attempting to bind VAO multiple times");
        #endif
        return;
    }
    if(indices.size() == 0)
    {

        Err::log("WARNING::VERTEXARRAY:: auto-generating EBO indices");

#ifndef NDEBUG
        //this is a silly check no one wants to make on a release build
        if(buffer.size() < 2)
        {
            Err::report("ERROR::VERTEXARRAY:: vertex buffer size too small");
            return;
        }
#endif
        for(unsigned int i = 2; i < buffer.size(); i+=3)
        {
            indices.push_back(i-2);
            indices.push_back(i-1);
            indices.push_back(i);
        }
    }



    //Run these two first because we need to get these values
    auto genVA = WDGL(GenVertexArrays, 1, &id);
    //DisplayManager::checkGLError("VertexArray::Load() -> glGenVertexArrays()");
    auto genBuf = WDGL(GenBuffers, 1, &ebid);
    //DisplayManager::checkGLError("VertexArray::Load() -> glGenBuffers()");
    Game::instance()->queueGLTask(genVA.Task());
    Game::instance()->queueGLTask(genBuf.Task());
    genVA.get();
    genBuf.get();

    GLTaskQueue q;
    std::vector< GLPackage<void> > v;

    v.push_back(WDGL(BindVertexArray, id));
    //DisplayManager::checkGLError("VertexArray::Load() -> glBindVertexArray()");

        if(buffer.Registered())
        {
            Err::log("WARNING::VERTEXARRAY:: Attempting to bind Vertex Buffer multiple times");
        }
        buffer.bindBuffer(v);
        v.push_back(WDGL(BindBuffer, GL_ELEMENT_ARRAY_BUFFER, ebid));
        //DisplayManager::checkGLError("VertexArray::Load() -> glBindBuffer()");
        v.push_back(WDGL(BufferData, GL_ELEMENT_ARRAY_BUFFER, static_cast<GLsizeiptr>(sizeof(unsigned int)*indices.size()),
                     indices.data(), static_cast<GLenum>(type)));
        //DisplayManager::checkGLError("VertexArray::Load() -> glBufferData()");



        //glVertexAttribPointer(index, size, type, normalized,
        //                      stride, first   );
        //Position
        v.push_back(WDGL(VertexAttribPointer, 0, 3, GL_FLOAT, GL_FALSE,
                              21 * sizeof(GLfloat), nullptr));
        v.push_back(WDGL(EnableVertexAttribArray, 0));

        //Normal
        v.push_back(WDGL(VertexAttribPointer, 1, 3, GL_FLOAT, GL_TRUE,
                              21 * sizeof(GLfloat), reinterpret_cast<GLvoid*>(3 * sizeof(GLfloat)) ));
        v.push_back(WDGL(EnableVertexAttribArray, 1));

        //UV
        v.push_back(WDGL(VertexAttribPointer, 2, 2, GL_FLOAT, GL_FALSE,
                              21 * sizeof(GLfloat), reinterpret_cast<GLvoid*>(6 * sizeof(GLfloat)) ));
        v.push_back(WDGL(EnableVertexAttribArray, 2));

        //Ambient Colour
        v.push_back(WDGL(VertexAttribPointer, 3, 4, GL_FLOAT, GL_FALSE,
                              21 * sizeof(GLfloat), reinterpret_cast<GLvoid*>(8 * sizeof(GLfloat)) ));
        v.push_back(WDGL(EnableVertexAttribArray, 3));

        //Diffuse Colour
        v.push_back(WDGL(VertexAttribPointer, 4, 4, GL_FLOAT, GL_FALSE,
                              21 * sizeof(GLfloat), reinterpret_cast<GLvoid*>(12 * sizeof(GLfloat)) ));
        v.push_back(WDGL(EnableVertexAttribArray, 4));

        //Specular Colour
        v.push_back(WDGL(VertexAttribPointer, 5, 4, GL_FLOAT, GL_FALSE,
                              21 * sizeof(GLfloat), reinterpret_cast<GLvoid*>(16 * sizeof(GLfloat)) ));
        v.push_back(WDGL(EnableVertexAttribArray, 5));

        //Shyniness
        v.push_back(WDGL(VertexAttribPointer, 6, 1, GL_FLOAT, GL_FALSE,
                              21 * sizeof(GLfloat), reinterpret_cast<GLvoid*>(20 * sizeof(GLfloat)) ));
        v.push_back(WDGL(EnableVertexAttribArray, 6));
        //DisplayManager::checkGLError("VertexArray::Load() -> glVertexAttribPointer()+glEnableVertexAttribArray()");



    v.push_back(WDGL(BindVertexArray, 0));
    //DisplayManager::checkGLError("VertexArray::Load() -> glBindVertexArray(0)");

    for(auto& task: v)
    {
        pushGLTask(q, task);
    }
    Game::instance()->queueGLTasks(std::move(q));
    for(auto& task: v)
    {
        task.get();
    }

}

void VertexArray::Bind() const noexcept
{
    auto task = WDGL(BindVertexArray, id);
    Game::instance()->queueGLTask(task.Task());
    task.get();
    //DisplayManager::checkGLError("VertexArray::Bind()");
}

void VertexArray::Unbind()
{
    auto task = WDGL(BindVertexArray, 0);
    Game::instance()->queueGLTask(task.Task());
    task.get();
    //DisplayManager::checkGLError("VertexArray::Bind()");
}

void VertexArray::appendIndices(ElementBuffer::Data& d)
{
    if(indices.empty())
    {
        indices = d;
    }
    else
    {
        indices.insert(indices.end(),d.begin(), d.end());
    }
}

void VertexArray::appendIndices(ElementBuffer::Data&& d)
{
    if(indices.empty())
    {
        indices = d;
    }
    else
    {
        indices.insert(indices.end(),d.begin(), d.end());
    }
}
