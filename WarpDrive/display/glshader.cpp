#include "WarpDrive/display/glshader.hpp"

#include <iostream>
#include <fstream>
#include <sstream>

#include "WarpDrive/basesystem/err.hpp"
#include "WarpDrive/basesystem/displaymanager.hpp"
#include "WarpDrive/basesystem/gltask.hpp"

GLShader::GLShader()
   :program{0},
    vertID{0},
    fragID{0},
    initialised{false},
    vertLoaded{false},
    fragLoaded{false}
{}

GLShader::GLShader(std::string vertex, std::string fragment)
    :program{0},
     vertID{0},
     fragID{0},
     initialised{false},
     vertLoaded{false},
     fragLoaded{false}
{
    load(vertex,fragment);
}

bool GLShader::load(std::string vertex, std::string fragment)
{
    return loadVertex(vertex) && loadFragment(fragment);
}

bool GLShader::loadVertex(std::string path)
{
    if(initialised || vertLoaded)
    {
        Err::log("ERROR::SHADER::VERTEX::SHADER_ALREADY_LOADED");
        return false;
    }

    std::string src;
    std::ifstream vertFile;
    try
    {
        vertFile.open(path);
        std::stringstream vertStream;

        vertStream << vertFile.rdbuf();

        vertFile.close();
        src = vertStream.str();
    }
    catch (std::ifstream::failure e)
    {
        Err::log("ERROR::SHADER::VERTEX::FILE_NOT_SUCCESFULLY_READ");
    }
    const GLchar* glsrc = src.c_str();
    GLint success = 0;
    GLint length[1] ={static_cast<GLint>(src.length())};
    GLchar info[512];


    //We need this first
    auto idPack = WDGL(CreateShader, GL_VERTEX_SHADER);
    Game::instance()->queueGLTask(idPack.Task());
    vertID = idPack.get();

    GLTaskQueue q;
    std::vector< GLPackage<void> > v;

    //DisplayManager::checkGLError("GLShader::loadVertex(" + std::to_string(program) + ") -> glCreateShader(GL_VERTEX_SHADER)");
    v.push_back(WDGL(ShaderSource, vertID, 1, &glsrc, length));
    //DisplayManager::checkGLError("GLShader::loadVertex(" + std::to_string(program) + ") -> glShaderSource()");
    v.push_back(WDGL(CompileShader, vertID));
    //DisplayManager::checkGLError("GLShader::loadVertex(" + std::to_string(program) + ") -> glCompileShader()");
    v.push_back(WDGL(GetShaderiv, vertID, GL_COMPILE_STATUS, &success));
    //DisplayManager::checkGLError("GLShader::loadVertex(" + std::to_string(program) + ") -> glGetShaderiv(GL_COMPILE_STATUS)");
    for(auto& task: v)
    {
        pushGLTask(q, task);
    }
    Game::instance()->queueGLTasks(std::move(q));
    for(auto& task: v)
    {
        task.get();
    }

    if(success == 0)
    {
        auto succPack = WDGL(GetShaderInfoLog, vertID, 512, nullptr, (GLchar*)info);
        Game::instance()->queueGLTask(succPack.Task());
        succPack.get();
        std::string error("ERROR::SHADER::VERTEX::COMPILATION_FAILED\n");
        error.append(info);
        Err::notify(error);
        return false;
    }
    else
    {
        vertLoaded = true;
        if(fragLoaded)
        {
            assemble();
        }
    }
    return true;
}

bool GLShader::loadFragment(std::string path)
{
    if(initialised || fragLoaded)
    {
        Err::log("ERROR::SHADER::FRAGMENT::SHADER_ALREADY_LOADED");
        return false;
    }

    std::string src;
    std::ifstream fragFile;
    try
    {
        fragFile.open(path);
        std::stringstream fragStream;

        fragStream << fragFile.rdbuf();

        fragFile.close();
        src = fragStream.str();
    }
    catch (std::ifstream::failure e)
    {
        Err::notify("ERROR::SHADER::FRAGMENT::FILE_NOT_SUCCESFULLY_READ");
    }
    const GLchar* glsrc = src.c_str();
    GLint success = 0;
    GLint length[1] ={static_cast<GLint>(src.length())};
    GLchar info[512];


    //We need this first
    auto idPack = WDGL(CreateShader, GL_FRAGMENT_SHADER);
    Game::instance()->queueGLTask(idPack.Task());
    fragID = idPack.get();

    GLTaskQueue q;
    std::vector< GLPackage<void> > v;

    v.push_back(WDGL(ShaderSource, fragID, 1, &glsrc, length));
    v.push_back(WDGL(CompileShader, fragID));
    v.push_back(WDGL(GetShaderiv, fragID, GL_COMPILE_STATUS, &success));

    for(auto& task: v)
    {
        pushGLTask(q, task);
    }
    Game::instance()->queueGLTasks(std::move(q));
    for(auto& task: v)
    {
        task.get();
    }

    if(success == 0)
    {
        auto succPack = WDGL(GetShaderInfoLog, fragID, 512, nullptr, (GLchar*)info);
        Game::instance()->queueGLTask(succPack.Task());
        succPack.get();
        std::string error("ERROR::SHADER::FRAGMENT::COMPILATION_FAILED\n");
        error.append(info);
        Err::notify(error);
        return false;
    }
    else
    {
        fragLoaded = true;
        if(vertLoaded)
        {
            assemble();
        }
    }
    return true;
}

bool GLShader::use() const
{
    if(!initialised)
    {
        return false;
    }
    else
    {
        auto pack = WDGL(UseProgram, program);
        Game::instance()->queueGLTask(pack.Task());
        pack.get();
        //DisplayManager::checkGLError("GLShader::use(" + std::to_string(program) + ")");
        return true;
    }
}

GLint GLShader::operator[](std::string uniform) const
{
    if(!initialised)
    {
        Err::log("ERROR::SHADER::PROGRAM::[] -> Attempted to read uniform of unitialised shader");
        return -1;
    }

    auto pack = WDGL(GetUniformLocation, program, uniform.c_str());
    Game::instance()->queueGLTask(pack.Task());
    return pack.get();
}

int GLShader::Program() const noexcept
{
    return initialised ? static_cast<int>(program) : -1;
}

GLShader::~GLShader()
{
    if(initialised)
    {
        auto pack = WDGL(DeleteProgram, program);
        Game::instance()->queueGLTask(pack.Task());
        pack.get();
        //DisplayManager::checkGLError("GLShader::~GLShader() -> glDeleteProgram()");

    }
}

void GLShader::assemble()
{
    GLint success = 0;
    GLchar info[512];

    GLTaskQueue q;
    std::vector< GLPackage<void> > v;

    auto programTask = WDGLv(CreateProgram);
    Game::instance()->queueGLTask(programTask.Task());
    program = programTask.get();
    //DisplayManager::checkGLError("GLShader::assemble() -> glCreateProgram()");
    v.push_back(WDGL(AttachShader, program, vertID));
    //DisplayManager::checkGLError("GLShader::assemble() -> glAttachShader(vert)");
    v.push_back(WDGL(AttachShader, program, fragID));
    //DisplayManager::checkGLError("GLShader::assemble() -> glAttachShader(frag)");

    v.push_back(WDGL(LinkProgram, program));
    //DisplayManager::checkGLError("GLShader::assemble() -> glLinkProgram()");

    v.push_back(WDGL(GetProgramiv, program, GL_LINK_STATUS, &success));

    for(auto& task: v)
    {
        pushGLTask(q, task);
    }
    Game::instance()->queueGLTasks(std::move(q));
    for(auto& task: v)
    {
        task.get();
    }

    //DisplayManager::checkGLError("GLShader::assemble() -> glGetProgramiv(GL_LINK_STATUS)");

    if (!success)
    {
        auto succPack = WDGL(GetProgramInfoLog, program, 512, nullptr, info);
        Game::instance()->queueGLTask(succPack.Task());
        succPack.get();
        std::string error ("ERROR::SHADER::PROGRAM::LINKING_FAILED\n");
        error.append(info);
        Err::notify(error);
    }
    else
    {
        initialised = true;
        auto delVert = WDGL(DeleteShader, vertID);
        //DisplayManager::checkGLError("GLShader::assemble() -> glDeleteShader(vert)");
        auto delFrag = WDGL(DeleteShader, fragID);
        //DisplayManager::checkGLError("GLShader::assemble() -> glDeleteShader(frag)");
        Game::instance()->queueGLTask(delVert.Task());
        Game::instance()->queueGLTask(delFrag.Task());
        delVert.get();
        delFrag.get();
    }

}
