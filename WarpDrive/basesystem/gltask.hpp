#ifndef WD_GLTASK_HPP_DEFINED
#define WD_GLTASK_HPP_DEFINED

#include <future>
#include <memory>
#include <utility>

#include "WarpDrive/basesystem/game.hpp"
#include "WarpDrive/basesystem/displaymanager.hpp"





template<typename T>
class GLPackage
{
        /*
         * This class is a fancy pair. What I want from it compared to a regular pair is:
         * 1 - No copy, only move throughout
         * 2 - Just some more descriptive names on the accessors
         *
         * It also needs to do double duty in case we've actually just called the function
         * instead of having it packaged for queueing.
         * To this end it relies on function operator bool
         */
    public:
        GLPackage(std::function<void()>&& t, std::future<T>&& f): task{std::move(t)}, future{std::move(f)} {}
        GLPackage(std::future<T>&& f): task{}, future{std::move(f)} {}
        GLPackage(const GLPackage& other) = delete;
        GLPackage(GLPackage&& other) = default;
        ~GLPackage() = default;

        explicit operator bool() const noexcept { return static_cast<bool>(task);}
        //void operator() () const {task();}
        std::function<void()>&& Task(){ return std::move(task);}
        std::future<T>& Future(){return future;}

        T get() {return future.get();}

    private:
        std::function<void()> task;
        std::future<T> future;
};

template<typename T>
void pushGLTask(GLTaskQueue& q, GLPackage<T>& p)
{
    if(static_cast<bool>(p) )
    {
        q.push_back(p.Task());
    }
}


// Code here adapted from
// https://stackoverflow.com/questions/34109641/c11-wrappers-for-opengl-calls

template <typename F, typename ...Args>
//return type here is a std::future of whatever type F returns (except void)
auto wdglcall(
    std::enable_if_t<!std::is_void<std::result_of_t<F(Args...)>>::value, const char> *text,
    int line,
    const char *file,
    F && f, Args &&... args
    )
{
    auto bound = std::bind(f, std::forward<Args>(args)...);
    std::string errMsg{std::string(file) + std::string("@") + std::to_string(line) + std::string(" -> ") + std::string(text)};
    std::packaged_task< decltype(bound()) (void)> task( [b{std::move(bound)}, msg{errMsg}]()
                                                        {
                                                            auto ret = b();
                                                            DisplayManager::instance()->checkGLError(msg);
                                                            return ret;
                                                        }   );
    auto fut = task.get_future();
    if(std::this_thread::get_id() != Game::instance()->mainThreadID())
    {
        /* If we're a different thread, we need the main one to call this
         * Here I move the task inside a lambda so I can try and mask the
         * type. If I moved the task itself, it'd have been a std::task<R,Args...>
         * which means I can't put different function signatures in the same
         * collection. All the lambdas should be void(void) tho
         */
        auto ptr = std::make_shared<decltype(task)>(std::move(task));
        auto func = [ p{ptr}]() mutable {(*p)();};
        //Game::instance()->queueGLTask(std::move(func));
        return GLPackage(std::move(func), std::move(fut));
        
    }
    else
    {
        task();
    }
    //return std::move(fut);
    return GLPackage(std::move(fut));

}

template <typename F, typename ...Args>
auto wdglcall(
    std::enable_if_t<std::is_void<std::result_of_t<F(Args...)>>::value, const char> *text,
    int line,
    const char *file,
    F && f, Args &&... args
    )
{
    auto bound = std::bind(f, std::forward<Args>(args)...);
    std::string errMsg{std::string(file) + std::string("@") + std::to_string(line) + std::string(" -> ") + std::string(text)};
    std::packaged_task< decltype(bound()) (void)> task( [b{std::move(bound)}, msg{errMsg}]()
                                                        {
                                                            b();
                                                            DisplayManager::instance()->checkGLError(msg);
                                                        }   );
    auto fut = task.get_future();
    if(std::this_thread::get_id() != Game::instance()->mainThreadID())
    {
        /* If we're a different thread, we need the main one to call this
         * Here I move the task inside a lambda so I can try and mask the
         * type. If I moved the task itself, it'd have been a std::task<R,Args...>
         * which means I can't put different function signatures in the same
         * collection. All the lambdas should be void(void) tho
         */
        auto ptr = std::make_shared<decltype(task)>(std::move(task));
        auto func = [ p{ptr}]() mutable {(*p)();};
        //Game::instance()->queueGLTask(std::move(func));
        return GLPackage(std::move(func), std::move(fut));    }
    else
    {
        task();
    }
    //return std::move(fut);
    return GLPackage(std::move(fut));
}


#define WDGL(fn, ...) wdglcall(#fn, __LINE__, __FILE__, gl##fn, __VA_ARGS__)
#define WDGLv(fn) wdglcall(#fn, __LINE__, __FILE__, gl##fn)
//#define WDGL(fn, ...) wdglcall(#fn, __LINE__, __FILE__, #fn, __VA_ARGS__)


#endif //WD_GLTASK_HPP_DEFINED
